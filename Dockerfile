FROM python:3.5
ENV PYTHONUNBUFFERED 1
EXPOSE 8000
COPY /requirements.txt /
RUN apt-get update -qy && apt-get install -y python-dev python-pip python-virtualenv python3-dev python3-pip libjpeg-dev libpq-dev imagemagick && pip install virtualenv
RUN virtualenv --python=/usr/bin/python3 /venv && \
    /venv/bin/pip install setuptools --upgrade && \
    /venv/bin/pip install -r /requirements.txt
CMD /venv/bin/python manage.py migrate && \
    /venv/bin/python manage.py loaddata initial && \
    /venv/bin/python manage.py runserver 0.0.0.0:8000
